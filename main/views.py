__author__ = 'db'
from django.shortcuts import render
from product.models import Product


def home(request):
    product_list = Product.objects.all()
    context = {"product_list": product_list}
    return render(request, "home.html", context)


def news(request):
    return render(request, "news.html")


def about_us(request):
    return render(request, "about_us.html")