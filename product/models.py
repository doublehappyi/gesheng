from django.db import models

# Create your models here.

class Product(models.Model):
    product_name = models.CharField(max_length=20);
    icon_url = models.CharField(max_length=200);

    def __unicode__(self):
        return self.product_name

# class News(models.Model):
#     news_title = models.CharField(max_length=20);
#     date = models.DateField();
#
#     def __unicode__(self):
#         return self.news_title